package com.epam.controller;

import com.epam.model.*;

import java.util.Random;

public class ControllerInst implements Controller {
    private Model model;

    public ControllerInst() {
        model = new Logic();
    }

    @Override
    public double makeProgramme() {
        double price = model.makeProgramme();
        if (price > 0) {
            System.out.println("Thanks for buying training programme for "
                    + price + "$");
        }
        System.out.println("Your training programme`s creation is in progress");
        try {
            Thread.sleep(generateRandom());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Your training programme is done!");
        return price;
    }

    @Override
    public double buySubscription() {
        double price = model.buySubscription();
        if (price > 0) {
            System.out.println("Thanks for buying subscription for "
                    + price + "$");
        }
        return price;
    }

    @Override
    public double buyWater() {
        double price = model.buyWater();
        if (price > 0) {
            System.out.println("Thanks for buying water for "
                    + price + "$");
        }
        return price;
    }

    @Override
    public double buySupplement() {
        double price = model.buySupplement();
        if (price > 0) {
            System.out.println("Thanks for buying sport supplement for " + price + "$");
        }
        return price;
    }

    @Override
    public double doExercise() {
        double timeTraining = model.doExercise();
        return timeTraining;
    }

    public int generateRandom() {
        int min = 1000;
        int max = 8000;
        int diff = max - min;
        Random random = new Random();
        int rand = random.nextInt(diff + 1);
        rand += min;
        return rand;
    }
}
