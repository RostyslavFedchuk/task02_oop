package com.epam.controller;

public interface Controller {
    double makeProgramme();

    double buySubscription();

    double buyWater();

    double buySupplement();

    double doExercise();
}
