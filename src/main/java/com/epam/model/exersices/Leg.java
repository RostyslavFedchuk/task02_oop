package com.epam.model.exersices;

public enum Leg {
    EXERSICE1 {
        public String toString() {
            return "Squad";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Leg press";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Deadlift";
        }
    }
}
