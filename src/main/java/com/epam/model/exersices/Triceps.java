package com.epam.model.exersices;

public enum Triceps {
    EXERSICE1 {
        public String toString() {
            return "Franch press";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Bars";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Wiring";
        }
    }
}
