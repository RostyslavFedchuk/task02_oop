package com.epam.model.exersices;

public enum Back {
    EXERSICE1 {
        public String toString() {
            return "Push up";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Press to the belt";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Deadlift";
        }
    }
}
