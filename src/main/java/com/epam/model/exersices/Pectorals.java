package com.epam.model.exersices;

public enum Pectorals {
    EXERSICE1 {
        public String toString() {
            return "Banch press";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Banch press(angle down)";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Banch press(angle up)";
        }
    }
}
