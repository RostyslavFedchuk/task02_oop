package com.epam.model.exersices;

public enum Delta {
    EXERSICE1 {
        public String toString() {
            return "Arnord press";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Smith press";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Wiring";
        }
    }
}
