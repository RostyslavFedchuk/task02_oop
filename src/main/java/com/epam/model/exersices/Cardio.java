package com.epam.model.exersices;

public enum Cardio {
    EXERSICE1 {
        public String toString() {
            return "Punching bag";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Running";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Running with obstacles";
        }
    }
}
