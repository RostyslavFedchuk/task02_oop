package com.epam.model.exersices;

public enum Biceps {
    EXERSICE1 {
        public String toString() {
            return "Crooked Barbell Press";
        }
    },
    EXERSICE2 {
        public String toString() {
            return "Barbell Press";
        }
    },
    EXERSICE3 {
        public String toString() {
            return "Hammers";
        }
    }
}
