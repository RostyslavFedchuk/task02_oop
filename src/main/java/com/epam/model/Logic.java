package com.epam.model;

import java.util.Scanner;

public class Logic implements Model {
    Data data;
    Muscle group;
    private static final Scanner SCANNER;

    static {
        SCANNER = new Scanner(System.in);
    }

    public Logic() {
        data = new Data();
        group = new Muscle(new Simulator());
    }

    public double makeProgramme() {
        return data.getPriceOfProgramme();
    }

    @Override
    public double buySubscription() {
        double price = 0;
        System.out.println("Today we have these offers: ");
        data.getSubscription();
        System.out.println("Enter what do you want to buy"
                + "(Choose one from the list)");
        String option = SCANNER.nextLine();
        if (data.getPriceSubscription(option) != -1) {
            price = data.getPriceSubscription(option);
        }
        return price;
    }

    @Override
    public double buyWater() {
        double price = 0;
        System.out.println("Today we have these offers: ");
        data.getWater();
        System.out.println("Enter what do you want to buy"
                + "(Choose one from the list)");
        String option = SCANNER.nextLine();
        if (data.getPriceWater(option) != -1) {
            price = data.getPriceWater(option);
        }
        return price;
    }

    @Override
    public double buySupplement() {
        double priceOfSupplement = 0;
        System.out.println("Today we have these offers: ");
        data.getSupplement();
        System.out.println("Enter what do you want to buy"
                + "(Choose one from the list)");
        String option = SCANNER.next();
        if (data.getPriceSupplement(option) != -1) {
            priceOfSupplement = data.getPriceSupplement(option);
        }
        return priceOfSupplement;
    }

    @Override
    public int doExercise() {
        int timeTraining = 0;
        group.getState();
        System.out.println("Enter which group of muscles do you want to train"
                + "(Choose one option from the list): ");
        String option = SCANNER.next();
        timeTraining = group.getMuscle(option);
        if (timeTraining != -1) {
            return timeTraining;
        }
        return timeTraining;
    }

}
