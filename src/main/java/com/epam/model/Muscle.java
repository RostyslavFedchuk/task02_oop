package com.epam.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Muscle {
    private Map<MuscleGroup, Integer> map;
    private Simulator simulator;

    public Muscle(Simulator simulator) {
        map = new LinkedHashMap<MuscleGroup, Integer>();
        this.simulator = simulator;
        MuscleGroup[] muscle = MuscleGroup.values();
        int[] duration = {45, 45, 30, 45, 40, 45, 30};
        for (int i = 0; i < muscle.length; i++) {
            map.put(muscle[i], duration[i]);
        }
    }

    public void getState() {
        System.out.println("You can train today these muscles: ");
        for (Map.Entry<MuscleGroup, Integer> entry : map.entrySet()) {
            MuscleGroup key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key + " - " + value + " mins");
        }
    }

    public int getMuscle(String option) {
        try {
            option = option.toUpperCase();
            simulator.doExercise(option, map);
            return map.get(MuscleGroup.valueOf(option));
        } catch (Exception e) {
            return -1;
        }
    }
}
