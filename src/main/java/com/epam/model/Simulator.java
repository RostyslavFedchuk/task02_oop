package com.epam.model;

import com.epam.model.exersices.*;

import java.util.Map;

public class Simulator {

    public void doExercise(String option, Map<MuscleGroup, Integer> map) {
        if (!map.containsKey(MuscleGroup.valueOf(option))) {
            System.out.println("There is no such muscle group "
                    + "or we don`t know it yet");
            return;
        }
        if ("PECTORALS".equals(option)) {
            printExercise(Pectorals.values(), option);
        } else if ("LEGS".equals(option)) {
            printExercise(Leg.values(), option);
        } else if ("BICEPS".equals(option)) {
            printExercise(Biceps.values(), option);
        } else if ("TRICEPS".equals(option)) {
            printExercise(Triceps.values(), option);
        } else if ("DELTA".equals(option)) {
            printExercise(Delta.values(), option);
        } else if ("BACK".equals(option)) {
            printExercise(Back.values(), option);
        } else if ("CARDIO".equals(option)) {
            printExercise(Cardio.values(), option);
        }
    }

    public void printExercise(Object[] muscleGroup, String option) {
        System.out.println("OK! " + option + "`s day. let it be! "
                + "I recommend you to do these exercises: ");
        int i = 1;
        for (Object entry : muscleGroup) {
            System.out.println(i + " - " + entry);
            i++;
        }
    }
}
