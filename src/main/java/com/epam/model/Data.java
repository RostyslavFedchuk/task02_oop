package com.epam.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Data {
    private double priceOfProgramme;
    private Map<String, Double> mapSupplement;
    private Map<String, Double> mapBottle;
    private Map<String, Double> mapSubscription;

    public Data() {
        priceOfProgramme = 15;
        mapSupplement = new LinkedHashMap<String, Double>();
        mapBottle = new LinkedHashMap<String, Double>();
        mapSubscription = new LinkedHashMap<String, Double>();
        String[] supplement = new String[]{"Protein", "Creatine",
                "AminoAcids", "NitricOxide", "Quit"};
        String[] sizeOfBootle = new String[]{"0.5 liter", "1 liter",
                "1.5 liter", "2 liter", "Quit"};
        String[] subscription = new String[]{"12 times in month",
                "limitless int month", "12 times for family", "Quit"};
        double[] priceSupplement = {10, 14, 12, 17, 0};
        double[] priceWater = {0.45, 0.64, 0.71, 0.89, 0};
        double[] priceSubscription = {12, 20, 18, 0};
        for (int i = 0; i < supplement.length; i++) {
            mapSupplement.put(supplement[i], priceSupplement[i]);
        }
        for (int i = 0; i < sizeOfBootle.length; i++) {
            mapBottle.put(sizeOfBootle[i], priceWater[i]);
        }
        for (int i = 0; i < subscription.length; i++) {
            mapSubscription.put(subscription[i], priceSubscription[i]);
        }
    }

    public void getSupplement() {
        System.out.println(mapSupplement.keySet());
    }

    public void getWater() {
        System.out.println(mapBottle.keySet());
    }

    public void getSubscription() {
        System.out.println(mapSubscription.keySet());
    }

    public double getPriceSupplement(String option) {
        try {
            return mapSupplement.get(option);
        } catch (Exception e) {
            return -1;
        }
    }

    public double getPriceWater(String option) {
        try {
            return mapBottle.get(option);
        } catch (Exception e) {
            return -1;
        }
    }

    public double getPriceSubscription(String option) {
        try {
            return mapSubscription.get(option);
        } catch (Exception e) {
            return -1;
        }
    }

    public double getPriceOfProgramme() {
        return priceOfProgramme;
    }
}
