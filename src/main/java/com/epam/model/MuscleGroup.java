package com.epam.model;

public enum MuscleGroup {
    PECTORALS,
    LEGS,
    BICEPS,
    TRICEPS,
    DELTA,
    BACK,
    CARDIO
}
