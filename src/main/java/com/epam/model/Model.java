package com.epam.model;

public interface Model {
    double makeProgramme();

    double buySubscription();

    double buyWater();

    double buySupplement();

    int doExercise();
}
