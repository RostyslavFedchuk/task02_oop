package com.epam.view;

import com.epam.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Instructor {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;
    private static final Scanner SCANNER;
    private double totalSum;
    private int countOfPurchase;
    private int timeTraining;

    static {
        SCANNER = new Scanner(System.in);
    }

    public Instructor(String name) {
        controller = new ControllerInst();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", " - Make a training programme");
        menu.put("2", " - Buy a subscription");
        menu.put("3", " - Buy water");
        menu.put("4", " - Buy sport supplement");
        menu.put("5", " - Do exercise");
        menu.put("Q", " - Quit");

        menuMethods = new LinkedHashMap<String, Printable>();
        menuMethods.put("1", this::pressButton1);
        menuMethods.put("2", this::pressButton2);
        menuMethods.put("3", this::pressButton3);
        menuMethods.put("4", this::pressButton4);
        menuMethods.put("5", this::pressButton5);
        menuMethods.put("Q", this::pressButtonQ);
        System.out.println("Hello! My name is " + name
                + ". What can I do for you?");
    }

    private void pressButton1() {
        double tmpPrice = controller.makeProgramme();
        if (tmpPrice > 0) {
            countOfPurchase++;
        }
        totalSum += tmpPrice;
    }

    private void pressButton2() {
        double tmpPrice = controller.buySubscription();
        if (tmpPrice > 0) {
            countOfPurchase++;
        }
        totalSum += tmpPrice;
    }

    private void pressButton3() {
        double tmpPrice = controller.buyWater();
        if (tmpPrice > 0) {
            countOfPurchase++;
        }
        totalSum += tmpPrice;
    }

    private void pressButton4() {
        double tmpPrice = controller.buySupplement();
        if (tmpPrice > 0) {
            countOfPurchase++;
        }
        totalSum += tmpPrice;
    }

    private void pressButton5() {
        this.timeTraining += controller.doExercise();
    }

    private void pressButtonQ() {
        if (countOfPurchase > 0) {
            System.out.println("You did " + countOfPurchase
                    + " purchases.\n"
                    + "Total sum of your purchases is: " + totalSum + "$");
        }
        if (timeTraining > 0) {
            System.out.println("You did great job! You spent "
                    + timeTraining + " minutes in the gym!");
        }
        System.out.println("Good luck! Thank`s for comming.");
    }

    public void printOffer() {
        System.out.println("___________Options___________");
        for (Map.Entry<String, String> entry : menu.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + value);
        }
        System.out.println("Choose one option:");
    }

    public void offer() {
        String key = "";
        do {
            printOffer();
            key = SCANNER.next().toUpperCase();
            try {
                menuMethods.get(key).print();
            } catch (Exception e) {
                System.out.println("Incorrect input! Please, try again!");
            }
        } while (!key.equals("Q"));
    }
}
